// Fill out your copyright notice in the Description page of Project Settings.


#include "HomeBase.h"
#include "Components/StaticMeshComponent.h"
#include "EnemyAI.h"
#include "TDGameMode.h"

// Sets default values
AHomeBase::AHomeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	SetRootComponent(StaticMesh);

	Health = 10;

	StaticMesh->OnComponentHit.AddDynamic(this, &AHomeBase::OnHit);
}

// Called when the game starts or when spawned
void AHomeBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AHomeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHomeBase::TakeDamage()
{
	this->Health--;
	OnDamageTaken.Broadcast(this);
}

void AHomeBase::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (AEnemyAI* enemy = Cast<AEnemyAI>(OtherActor))
	{
		//this->Health -= 1;
	}

	//Event that core takes damage
	//Actor is not called by C++, pwede Blueprint
}
