// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAISpawner.h"
#include "Components/SceneComponent.h"
#include "TimerManager.h"
#include "EnemyAI.h"
#include "EnemyAIController.h"
#include "Kismet/GameplayStatics.h"
#include "Waypoint.h"

// Sets default values
AEnemyAISpawner::AEnemyAISpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);
}

// Called when the game starts or when spawned
void AEnemyAISpawner::BeginPlay()
{
	Super::BeginPlay();
	SpawnEnemies();
}

// Called every frame
void AEnemyAISpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemyAISpawner::SpawnEnemies()
{
	FActorSpawnParameters spawnInfo;
	GetWorld()->SpawnActor<AEnemyAI>(Enemy, this->GetActorLocation(), this->GetActorRotation(), spawnInfo);
}

