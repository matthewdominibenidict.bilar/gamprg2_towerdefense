// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnableEnemyTypes")
		TArray<TSubclassOf<class AEnemyAI>> Enemies;

	//Example Get Random Enemy

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IncreaseHealth")
		int32 increaseHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WaveNumber")
		int32 currentWaveNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WaveNumber")
		int32 finalWaveNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnDelay")
		int32 spawnDelay;

	//Game Mode Handles the waves
};
