// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "EnemyAI.h"
#include "EnemyAISpawner.h"

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	AEnemyAI* enemyAI = Cast<AEnemyAI>(GetPawn());
	
	if (enemyAI)
	{
		enemyAI->MoveToWaypoint();
	}
}