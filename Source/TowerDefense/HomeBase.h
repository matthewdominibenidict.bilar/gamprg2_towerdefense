// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HomeBase.generated.h"

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTakeDamage, class AHomeBase*, OnDamageTaken);

UCLASS()
class TOWERDEFENSE_API AHomeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AHomeBase();

	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HomeBase Health")
		int32 Health;

	UFUNCTION(BlueprintCallable, Category = "TakeDamage")
		void TakeDamage();

	UPROPERTY(BlueprintAssignable, Category = "EventDispatcher")
		FTakeDamage OnDamageTaken;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};
