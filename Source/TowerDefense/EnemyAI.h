// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyAI.generated.h"

UCLASS()
class TOWERDEFENSE_API AEnemyAI : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemyAI();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void MoveToWaypoint();

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "True"))
	int CurrentWaypoint;

	TArray<AActor*> Waypoints;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy Health")
		int32 Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy MovementSpeed")
		int32 moveSpeed;

	//Combine Together (false - true) combo.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ground or Flying")
		bool isGround;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Normal or Boss")
		bool isNormal;

	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION(BlueprintImplementableEvent)
		void OnTrigger(class AHomeBase* HomeBase);
};
