// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemyAISpawner.generated.h"

UDELEGATE(BlueprintAuthorityOnly)
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSpawnEnemy, class AEnemyAISpawner*, EnemySpawn);

UCLASS()
class TOWERDEFENSE_API AEnemyAISpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemyAISpawner();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintAssignable)
		FSpawnEnemy SpawnEnemy;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "True"))
		int WayPointStart;

	UPROPERTY(EditAnywhere, Category = "WayPoints")
		TArray<class AActor*> WayPoints;

	UPROPERTY(EditAnywhere, Category = "SpawnableEnemies")
		TSubclassOf<class AEnemyAI> Enemy;

private:
	void SpawnEnemies();
	FTimerHandle timeDelay;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* SceneComponent;

	//Retain - (Main Copy in Game Mode) - Spawner Receives Copy
	//Reference to Wave Data
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wave Data Asset")
		UDataAsset* CurrentWave;

	//TArray of Waypoints
};
