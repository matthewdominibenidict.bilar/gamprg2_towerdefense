// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAI.h"
#include "EnemyAIController.h"
#include "Waypoint.h"
#include "Kismet/GameplayStatics.h"
#include "HomeBase.h"
#include "Components/CapsuleComponent.h"
#include "EnemyAISpawner.h"

// Sets default values
AEnemyAI::AEnemyAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEnemyAI::BeginPlay()
{
	Super::BeginPlay();	

	//Something to give to the waypoint to the spawner
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWaypoint::StaticClass(), Waypoints);
	MoveToWaypoint();

	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &AEnemyAI::OnHit);
}

// Called every frame
void AEnemyAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemyAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


void AEnemyAI::MoveToWaypoint()
{
	AEnemyAIController* enemyController = Cast<AEnemyAIController>(GetController());

	if (enemyController)
	{
		if (CurrentWaypoint <= Waypoints.Num())
		{
			for (AActor* Waypoint : Waypoints)
			{
				AWaypoint* WaypointItr = Cast<AWaypoint>(Waypoint);

				if (WaypointItr->GetWaypointOrder() == CurrentWaypoint)
				{
					enemyController->MoveToActor(WaypointItr, 5.f, false);
					CurrentWaypoint++;
					break;
				}
			}
		}
	}
}

void AEnemyAI::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (AHomeBase* homeBase = Cast<AHomeBase>(OtherActor))
	{
		OnTrigger(homeBase);
		this->Destroy();
	}
}

